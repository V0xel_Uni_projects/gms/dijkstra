#include <cstdio>
#include <cassert>
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>

constexpr int INF = std::numeric_limits<int>::max();

struct Node
{
	Node(const int n, const int w) :
		index(n), weight(w)
	{}

	int index;
	int weight;
};

using graph = std::vector< std::vector<Node> >;
using dV = std::vector< std::pair<bool, int> >; //true - if processed

int runDijkstra(const int begin, const int end, graph& graph)
{
	dV costs(graph.size(), { false, INF });
	costs[begin].second = 0;
	do 
	{
		const auto minEl = std::min_element(costs.begin(), costs.end());
		const auto minIndex = std::distance(costs.begin(), minEl);
		minEl->first = true; //marking as processed
		//Loop through all neighbours of the found V
		for (auto& n : graph[minIndex])
		{
			if (costs[n.index].first)
			{
				continue;
			}
			if (costs[n.index].second > costs[minIndex].second + n.weight)
			{
				costs[n.index].second = costs[minIndex].second + n.weight;
			}
		}
		
	} while (!costs[end].first);

	return costs[end].second;
}

int main()
{
	FILE* input = nullptr;
	const errno_t errInput = fopen_s(&input, "./tests/input", "r");
	FILE* output = nullptr;
	const errno_t errOutput = fopen_s(&output, "./tests/main_out", "w");
	assert(errInput == 0 && "Failed to open file");
	assert(errOutput == 0 && "Failed to create file");

	int tests = 0, cities = 0;
	fscanf_s(input, "%d%*c", &tests);

	//All tests loop
	for (int t = 0; t < tests; ++t)
	{
		std::unordered_map<std::string, int>cityNames;
		fscanf_s(input, "%d%*c", &cities);
		graph graph(cities);
		char temp[11] = {};
		//Adding cities to graph and keying their names to index
		int tempNext = 0, tempWeight = 0, neighbours = 0;
		for (int i = 0; i < cities; ++i)
		{
			fscanf_s(input, "%[^\n]%*c", &temp, 11);
			cityNames[temp] = i;

			neighbours = 0;
			fscanf_s(input, "%d%*c", &neighbours);

			for (int j = 0; j < neighbours; ++j)
			{
				fscanf_s(input, "%d %d%*c", &tempNext, &tempWeight);
				graph[i].emplace_back(tempNext - 1, tempWeight); //-1 to get 0..n indexing
			}
		}

		int numOfSolutions = 0;
		fscanf_s(input, "%d%*c", &numOfSolutions);
		std::vector<int>solutions(numOfSolutions, 0);

		for (auto& solution : solutions)
		{
			fscanf_s(input, "%[^ ]%*c", &temp, 11);
			std::string nameStart(temp);
			fscanf_s(input, "%[^\n]%*c", &temp, 11);
			std::string nameEnd(temp);

			solution = runDijkstra(cityNames[nameStart], cityNames[nameEnd], graph);
			fprintf_s(output, "%d\n", solution);
		}
	}
	
	fclose(input);
	fclose(output);
	return 0;
}